-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `mydb` DEFAULT CHARACTER SET utf8 ;
USE `mydb` ;

-- -----------------------------------------------------
-- Table `mydb`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`country`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`country` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`city`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`city` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `country_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_city_country_idx` (`country_id` ASC) VISIBLE,
  CONSTRAINT `fk_city_country`
    FOREIGN KEY (`country_id`)
    REFERENCES `mydb`.`country` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`street`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`street` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(60) NOT NULL,
  `city_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_street_city1_idx` (`city_id` ASC) VISIBLE,
  CONSTRAINT `fk_street_city1`
    FOREIGN KEY (`city_id`)
    REFERENCES `mydb`.`city` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`address`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`address` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `building_number` INT NOT NULL,
  `flat_number` INT NULL,
  `street_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_address_street1_idx` (`street_id` ASC) VISIBLE,
  CONSTRAINT `fk_address_street1`
    FOREIGN KEY (`street_id`)
    REFERENCES `mydb`.`street` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`user`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`user` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `photo_path` VARCHAR(255) NULL,
  `address_id` INT NOT NULL,
  `money` DECIMAL(9,2) NOT NULL DEFAULT 0.0,
  PRIMARY KEY (`id`),
  INDEX `fk_user_data_address1_idx` (`address_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_data_address1`
    FOREIGN KEY (`address_id`)
    REFERENCES `mydb`.`address` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`credential`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`credential` (
  `user_id` INT NOT NULL,
  `login` VARCHAR(20) NOT NULL,
  `pass` VARCHAR(20) NOT NULL,
  INDEX `fk_credential_user_data1_idx` (`user_id` ASC) VISIBLE,
  PRIMARY KEY (`user_id`),
  CONSTRAINT `fk_credential_user_data1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`email`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`email` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `email` VARCHAR(60) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_email_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_email_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`document_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`document_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`document`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`document` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `file_path` VARCHAR(255) NOT NULL,
  `document_type_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_document_document_type1_idx` (`document_type_id` ASC) VISIBLE,
  INDEX `fk_document_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_document_document_type1`
    FOREIGN KEY (`document_type_id`)
    REFERENCES `mydb`.`document_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_document_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`phone_number`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`phone_number` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `number` VARCHAR(45) NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_phone_number_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_phone_number_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`apartment_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`apartment_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`apartment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`apartment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `amount_of_guest` INT NOT NULL,
  `amount_bedroom` INT NOT NULL,
  `amount_of_bed` INT NOT NULL,
  `amount_of_bathroom` INT NOT NULL,
  `apartment_type_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `price_per_day` DECIMAL(7,2) NOT NULL,
  `description` VARCHAR(400) NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_apartment_apartment_type1_idx` (`apartment_type_id` ASC) VISIBLE,
  INDEX `fk_apartment_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_apartment_apartment_type1`
    FOREIGN KEY (`apartment_type_id`)
    REFERENCES `mydb`.`apartment_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_apartment_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`feedback`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`feedback` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `text` VARCHAR(400) NOT NULL,
  `apartment_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_feedback_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_feedback_user1_idx` (`user_id` ASC) VISIBLE,
  CONSTRAINT `fk_feedback_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `mydb`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_feedback_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`rating_type`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`rating_type` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`rating`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`rating` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `user_id` INT NOT NULL,
  `apartment_id` INT NOT NULL,
  `value` INT(1) NOT NULL,
  `rating_type_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_rating_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_rating_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_rating_rating_type1_idx` (`rating_type_id` ASC) VISIBLE,
  CONSTRAINT `fk_rating_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rating_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `mydb`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_rating_rating_type1`
    FOREIGN KEY (`rating_type_id`)
    REFERENCES `mydb`.`rating_type` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`apartment_photo`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`apartment_photo` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `path` VARCHAR(255) NOT NULL,
  `apartment_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_apartment_photo_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  CONSTRAINT `fk_apartment_photo_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `mydb`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`convenience`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`convenience` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `type` VARCHAR(100) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`apartment_convenience`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`apartment_convenience` (
  `apartment_id` INT NOT NULL,
  `convenience_id` INT NOT NULL,
  `presence` TINYINT NOT NULL,
  PRIMARY KEY (`apartment_id`, `convenience_id`),
  INDEX `fk_convenience_has_apartment_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_convenience_has_apartment_convenience1_idx` (`convenience_id` ASC) VISIBLE,
  CONSTRAINT `fk_convenience_has_apartment_convenience1`
    FOREIGN KEY (`convenience_id`)
    REFERENCES `mydb`.`convenience` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_convenience_has_apartment_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `mydb`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`reservation_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`reservation_status` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`reservation`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`reservation` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `start_date` DATE NOT NULL,
  `end_date` DATE NOT NULL,
  `amount_of_guest` INT NOT NULL,
  `apartment_id` INT NOT NULL,
  `user_id` INT NOT NULL,
  `reservation_status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_reservation_apartment1_idx` (`apartment_id` ASC) VISIBLE,
  INDEX `fk_reservation_user1_idx` (`user_id` ASC) VISIBLE,
  INDEX `fk_reservation_reservation_status1_idx` (`reservation_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_reservation_apartment1`
    FOREIGN KEY (`apartment_id`)
    REFERENCES `mydb`.`apartment` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservation_user1`
    FOREIGN KEY (`user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_reservation_reservation_status1`
    FOREIGN KEY (`reservation_status_id`)
    REFERENCES `mydb`.`reservation_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`payment_status`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`payment_status` (
  `id` INT NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `mydb`.`payment`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `mydb`.`payment` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `from_user_id` INT NOT NULL,
  `to_user_id` INT NOT NULL,
  `payment_status_id` INT NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_payment_user1_idx` (`from_user_id` ASC) VISIBLE,
  INDEX `fk_payment_user2_idx` (`to_user_id` ASC) VISIBLE,
  INDEX `fk_payment_payment_status1_idx` (`payment_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_payment_user1`
    FOREIGN KEY (`from_user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_user2`
    FOREIGN KEY (`to_user_id`)
    REFERENCES `mydb`.`user` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_payment_payment_status1`
    FOREIGN KEY (`payment_status_id`)
    REFERENCES `mydb`.`payment_status` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
